---
layout: default
---

# Privacy

This site uses YouTube Embeds, Twitter Embeds, and GitHub Sites, which might
collect information about visitors. If you would like to opt-out, please visit
the following sites:

-   **Twitter:** [Opt-Out](https://support.twitter.com/articles/20169453)
-   **YouTube:** [Opt-Out](https://support.google.com/ads/answer/2662922?hl=en)
-   **Google Analytics:** [Opt-Out](https://tools.google.com/dlpage/gaoptout)
-   **Rollbar:** No known opt-out - Only information collected is browser version
      if any JavaScript errors occur.
-   **GitLab Pages:** No known information collected.
