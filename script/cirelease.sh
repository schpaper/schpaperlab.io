#!/bin/sh

echo "build_revision:" $(git rev-parse HEAD) > _data/git.yml
bundle exec jekyll build -d public
