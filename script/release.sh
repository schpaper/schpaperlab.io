#!/bin/sh

git archive --format zip --output template.zip HEAD
cp template.zip template-upgrade.zip
zip -d template-upgrade.zip _data/debug.yml _data/header.yml icons/* \
  _data/birthdays.yml _data/wordofthemonth.yml _announcements/* \
  _clubs/* _sports/* _posts/* \
  _config.yml browserconfig.xml favicon.ico humans.txt index.html \
  google*.html manifest.json README.md
